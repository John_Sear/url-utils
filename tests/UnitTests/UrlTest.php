<?php

declare(strict_types=1);

namespace JohnSear\UrlUtils\Tests\UnitTests;

use JohnSear\UrlUtils\Url;
use JohnSear\UrlUtils\UrlParser;
use PHPUnit\Framework\TestCase;

class UrlTest extends TestCase
{
    /**
     * @group ignore
     */
    public function testUrlsProvider(): array
    {
        return [
            'url 1' => [
                [
                    'scheme'     => 'https',
                    'user'       => 'username',
                    'pass'       => 'password',
                    'host'       => 'sub1.sub2.sub3.example.com',
                    'port'       => 443,
                    'path'       => '/path/to/page',
                    'query'      => 'foo=bar&bla=blubb',
                    'fragment'   => 'test-fragment',
                    'subdomain'  => 'sub1.sub2.sub3',
                    'domainname' => 'example',
                    'tld'        => 'com'
                ]
            ],
            'url 2' => [
                [
                    'host'       => 'www.example.com',
                    'port'       => 443,
                ]
            ]
        ];
    }

    /**
     * @group ignore
     */
    public function testUrlBuilder(array $urlParts): string
    {
        $urlParts = $this->prepareTestUrlParts($urlParts);

        $scheme     = $urlParts['scheme'];
        $user       = $urlParts['user'];
        $pass       = $urlParts['pass'];
        $host       = $urlParts['host'];
        $port       = $urlParts['port'];
        $path       = $urlParts['path'];
        $query      = $urlParts['query'];
        $fragment   = $urlParts['fragment'];

        $subdomain  = $urlParts['subdomain'];
        $domainname = $urlParts['domainname'];
        $tld        = $urlParts['tld'];

        $ssl         = ($scheme === 'https');
        $protocol    = $scheme . '://';
        $credentials = ($user !== '' && $pass !== '') ? $user . ':' . $pass . '@' : '';
        $port        = ( $port === 0 || ( ! $ssl && $port === 80 ) || ( $ssl && $port === 443 ) ) ? '' : ':' . $port;
        $host        = $credentials . $host . $port;
        $requestUri  = $path . (($query !== '') ? '?' . $query : '') . (($fragment !== '') ? '#' . $fragment : '');

        return $protocol . $host . $requestUri;
    }

    public function prepareTestUrlParts(array $urlParts): array
    {
        $scheme     = $urlParts['scheme']      ?? Url::SCHEME_DEFAULT;
        $user       = $urlParts['user']        ?? '';
        $pass       = $urlParts['pass']        ?? '';
        $host       = $urlParts['host']        ?? '';
        $port       = (int) ($urlParts['port'] ?? (($scheme === Url::SCHEME_SECURE) ? Url::PORT_SECURE : Url::PORT_DEFAULT));
        $path       = $urlParts['path']        ?? '/';
        $query      = $urlParts['query']       ?? '';
        $fragment   = $urlParts['fragment']    ?? '';

        if($port === Url::PORT_SECURE && $scheme !== Url::SCHEME_SECURE) {
            $scheme = Url::SCHEME_SECURE;
        }
        if($port === Url::PORT_DEFAULT && $scheme !== Url::SCHEME_DEFAULT) {
            $scheme = Url::SCHEME_DEFAULT;
        }

        $requestUri = $path . ($query !== '' ? '?' . $query : '');
        $file = basename($requestUri, "?". $query);

        $subdomain  = $urlParts['subdomain']   ?? ($host !== '' ? UrlParser::getSubdomain('//' . $host) : '');
        $domainname = $urlParts['domainname']  ?? ($host !== '' ? UrlParser::getDomainName('//' . $host) : '');
        $tld        = $urlParts['tld']         ?? ($host !== '' ? UrlParser::getTopLevelDomain('//' . $host) : '');

         return [
            'scheme'     => $scheme,
            'user'       => $user,
            'pass'       => $pass,
            'host'       => $host,
            'port'       => $port,
            'path'       => $path,
            'query'      => $query,
            'fragment'   => $fragment,
            'file'       => $file,
            'subdomain'  => $subdomain,
            'domainname' => $domainname,
            'tld'        => $tld
        ];
    }

    public function testCurrentIsString(): void
    {
        $currentUrl = Url::current();

        echo 'Test current() = ' . $currentUrl . ' (expected: is string)';

        $this->assertIsString($currentUrl);
    }

    /**
     * @dataProvider testUrlsProvider
     */
    public function test_getScheme_containsExpected(array $urlParts): void
    {
        $urlParts = $this->prepareTestUrlParts($urlParts);

        $testUrl     = $this->testUrlBuilder($urlParts);
        $testUrlPart = $urlParts['scheme'] ?? '';

        $expected = $testUrlPart;
        $actual   = (new Url($testUrl))->getScheme();

        echo 'Test getScheme() = ' . $actual . ' (expected: ' . $expected . ')';

        $this->assertEquals($expected, $actual);
    }

    /**
     * @dataProvider testUrlsProvider
     */
    public function test_getUser_containsExpected(array $urlParts): void
    {
        $urlParts = $this->prepareTestUrlParts($urlParts);

        $testUrl     = $this->testUrlBuilder($urlParts);
        $testUrlPart = $urlParts['user'] ?? '';

        $expected = $testUrlPart;
        $actual   = (new Url($testUrl))->getUser();

        echo 'Test getUser() = ' . $actual . ' (expected: ' . $expected . ')';

        $this->assertEquals($expected, $actual);
    }

    /**
     * @dataProvider testUrlsProvider
     */
    public function test_getPassword_containsExpected(array $urlParts): void
    {
        $urlParts = $this->prepareTestUrlParts($urlParts);

        $testUrl     = $this->testUrlBuilder($urlParts);
        $testUrlPart = $urlParts['pass'] ?? '';

        $expected = $testUrlPart;
        $actual   = (new Url($testUrl))->getPass();

        echo 'Test getPass() = ' . $actual . ' (expected: ' . $expected . ')';

        $this->assertEquals($expected, $actual);
    }

    /**
     * @dataProvider testUrlsProvider
     */
    public function test_getHost_containsExpected(array $urlParts): void
    {
        $urlParts = $this->prepareTestUrlParts($urlParts);

        $testUrl     = $this->testUrlBuilder($urlParts);
        $testUrlPart = $urlParts['host'] ?? '';

        $expected = $testUrlPart;
        $actual   = (new Url($testUrl))->getHost();

        echo 'Test getHost() = ' . $actual . ' (expected: ' . $expected . ')';

        $this->assertEquals($expected, $actual);
    }

    /**
     * @dataProvider testUrlsProvider
     */
    public function test_getPort_containsExpected(array $urlParts): void
    {
        $urlParts = $this->prepareTestUrlParts($urlParts);

        $testUrl     = $this->testUrlBuilder($urlParts);
        $testUrlPart = $urlParts['port'] ?? '';

        $expected = $testUrlPart;
        $actual   = (new Url($testUrl))->getPort();

        echo 'Test getPort() = ' . $actual . ' (expected: ' . $expected . ')';

        $this->assertEquals($expected, $actual);
    }

    /**
     * @dataProvider testUrlsProvider
     */
    public function test_getPath_containsExpected(array $urlParts): void
    {
        $urlParts = $this->prepareTestUrlParts($urlParts);

        $testUrl     = $this->testUrlBuilder($urlParts);
        $testUrlPart = $urlParts['path'] ?? '';

        $expected = $testUrlPart;
        $actual   = (new Url($testUrl))->getPath();

        echo 'Test getPath() = ' . $actual . ' (expected: ' . $expected . ')';

        $this->assertEquals($expected, $actual);
    }

    /**
     * @dataProvider testUrlsProvider
     */
    public function test_getQuery_containsExpected(array $urlParts): void
    {
        $urlParts = $this->prepareTestUrlParts($urlParts);

        $testUrl     = $this->testUrlBuilder($urlParts);
        $testUrlPart = $urlParts['query'] ?? '';

        $expected = $testUrlPart;
        $actual   = (new Url($testUrl))->getQuery();

        echo 'Test getQuery() = ' . $actual . ' (expected: ' . $expected . ')';

        $this->assertEquals($expected, $actual);
    }

    /**
     * @dataProvider testUrlsProvider
     */
    public function test_getFragment_containsExpected(array $urlParts): void
    {
        $urlParts = $this->prepareTestUrlParts($urlParts);

        $testUrl     = $this->testUrlBuilder($urlParts);
        $testUrlPart = $urlParts['fragment'] ?? '';

        $expected = $testUrlPart;
        $actual   = (new Url($testUrl))->getFragment();

        echo 'Test getFragment() = ' . $actual . ' (expected: ' . $expected . ')';

        $this->assertEquals($expected, $actual);
    }

    /**
     * @dataProvider testUrlsProvider
     */
    public function test_getSubdomain_containsExpected(array $urlParts): void
    {
        $urlParts = $this->prepareTestUrlParts($urlParts);

        $testUrl     = $this->testUrlBuilder($urlParts);
        $testUrlPart = $urlParts['subdomain'] ?? '';

        $expected = $testUrlPart;
        $actual   = (new Url($testUrl))->getSubdomain();

        echo 'Test getSubdomain() = ' . $actual . ' (expected: ' . $expected . ')';

        $this->assertEquals($expected, $actual);
    }

    /**
     * @dataProvider testUrlsProvider
     */
    public function test_getSubdomains_containsExpected(array $urlParts): void
    {
        $urlParts = $this->prepareTestUrlParts($urlParts);

        $testUrl     = $this->testUrlBuilder($urlParts);
        $testUrlPart = explode('.', ($urlParts['subdomain'] ?? ''));

        $expected = $testUrlPart;
        $actual   = (new Url($testUrl))->getSubdomains();

        echo str_replace(["\r\n", "\r", "\n", '   '], '', 'Test getSubdomain() = ' . print_r($actual, true) . ' (expected: ' . print_r($expected, true) . ')');

        $this->assertEquals($expected, $actual);
    }

    /**
     * @dataProvider testUrlsProvider
     */
    public function test_getDomainname_containsExpected(array $urlParts): void
    {
        $urlParts = $this->prepareTestUrlParts($urlParts);

        $testUrl     = $this->testUrlBuilder($urlParts);
        $testUrlPart = $urlParts['domainname'] ?? '';

        $expected = $testUrlPart;
        $actual   = (new Url($testUrl))->getDomainname();

        echo 'Test getDomainname() = ' . $actual . ' (expected: ' . $expected . ')';

        $this->assertEquals($expected, $actual);
    }

    /**
     * @dataProvider testUrlsProvider
     */
    public function test_getTld_containsExpected(array $urlParts): void
    {
        $urlParts = $this->prepareTestUrlParts($urlParts);

        $testUrl     = $this->testUrlBuilder($urlParts);
        $testUrlPart = $urlParts['tld'] ?? '';

        $expected = $testUrlPart;
        $actual   = (new Url($testUrl))->getTld();

        echo 'Test getTld() = ' . $actual . ' (expected: ' . $expected . ')';

        $this->assertEquals($expected, $actual);
    }

    /**
     * @dataProvider testUrlsProvider
     */
    public function test_toString_containsExpected(array $urlParts): void
    {
        $urlParts = $this->prepareTestUrlParts($urlParts);

        $testUrl     = $this->testUrlBuilder($urlParts);

        $expected = $testUrl;
        $actual   = (new Url($testUrl))->__toString();

        echo 'Test __toString() = ' . $actual . ' (expected: ' . $expected . ')';

        $this->assertEquals($expected, $actual);
    }

    /**
     * @dataProvider testUrlsProvider
     */
    public function test_toArray_containsExpected(array $urlParts): void
    {
        $urlParts = $this->prepareTestUrlParts($urlParts);

        $testUrl     = $this->testUrlBuilder($urlParts);

        $expected = $urlParts;
        $actual   = (new Url($testUrl))->__toArray();

        echo str_replace(["\r\n", "\r", "\n", '   '], '', 'Test __toArray() = ' . print_r($actual, true) . ' (expected: ' . print_r($expected, true) . ')');

        $this->assertEquals($expected, $actual);
    }
}
