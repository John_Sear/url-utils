# JohnSear URL Utils - Documentation - Url Builder

## Build Url by Url Parameters

```php
require __DIR__ . '/vendor/autoload.php';

use JohnSear\UrlUtils\UrlBuilder;

$urlBuilder = new UrlBuilder('https', 'www.example.com');

$urlBuilder->setPath('/path/to/page');
$urlBuilder->addQueryParam('foo', 'bar');
// see more methods in Class ...

echo $urlBuilder->build(); // i.e. "https://www.example.com/path/to/page?foo=bar"

// more settings can be done after every build
$urlBuilder->addQueryParam('one', 'two');
$urlBuilder->addQueryParam('three'); // no value is needed
$urlBuilder->addQueryParam('four', 'five');

echo $urlBuilder->build(); // i.e. "https://www.example.com/path/to/page?foo=bar&one=two&three&four=five"

$urlBuilder->removeQueryParam('three'); // query params can also be removed

echo $urlBuilder->build(); // i.e. "https://www.example.com/path/to/page?foo=bar&one=two&four=five"

$urlBuilder->setFragment('my-fragment'); // Fragments i.e. for anchors on page can be added

echo $urlBuilder->build(); // i.e. "https://www.example.com/path/to/page?foo=bar&one=two&four=five#my-fragment"
```

## Basic HTTP Authentication

To build Urls including basic authentication with the _RFC 7617_ Standard (as desrciped [here](https://www.rfc-editor.org/info/rfc7617))
can be add to the UrlBulder with the `JohnSear\UrlUtils\UrlBasicAuthCredentials` Object with the `setCredentials()` Method.

```php
require __DIR__ . '/vendor/autoload.php';

use JohnSear\UrlUtils\UrlBasicAuthCredentials;
use JohnSear\UrlUtils\UrlBuilder;

$urlBuilder = new UrlBuilder('https', 'www.example.com');

$credentials = new UrlBasicAuthCredentials();
$credentials->setUser('username');
$credentials->setPass('highsecurepass');

$urlBuilder->setCredentials($credentials);

$urlBuilder->setPath('/path/to/page');

echo $urlBuilder->build(); // i.e. "https://username:highsecurepass@www.example.com/path/to/page"
```
> In build process credentials will be [urlencode](https://www.php.net/manual/en/function.urlencode.php)d, to ensure save usage in url!
