# JohnSear URL Utils - Documentation - Url Checker

UrlChecker Methods are _static_, so no initialization is needed!

> Here the PHP Extensions `sockets` and `curl` are required!

```php
require __DIR__ . '/vendor/autoload.php';

use JohnSear\UrlUtils\UrlChecker;

$url = 'https://www.example.com/path/to/page';

echo UrlChecker::getHttpStatusCode($url); // i.e. "200"
echo UrlChecker::isIpFromUrlResolvable($url); // i.e. TRUE
echo UrlChecker::isUrlReachable($url, 20); // i.e. TRUE, second Parameter sets Timeout in seconds
// see more methods in Class ...
```
