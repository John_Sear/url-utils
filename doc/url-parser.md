# JohnSear URL Utils - Documentation - Url Parser

## Basic Usage

UrlParser Methods are _static_, so no initialization is needed!

```php
require __DIR__ . '/vendor/autoload.php';

use JohnSear\UrlUtils\UrlParser;

$url = 'https://username:highsecurepass@sub2.sub1.example.com:7580/path/to/page.html?foo=bar&one=two&tree&four=five#my-fragment';

// get all parsed parts as array
$parsedUrl = UrlParser::parseUrl($url);

// returns an Array like:
// Array
// (
//     [scheme] => https
//     [user] => username
//     [pass] => highsecurepass
//     [host] => sub2.sub1.example.com
//     [port] => 7580
//     [path] => /path/to/page.html
//     [query] => foo=bar&one=two&four=five
//     [fragment] => my-fragment
//     [file] => page.html
//     [subdomain] => sub2.sub1
//     [domainname] => example
//     [tld] => com
// )

echo UrlParser::getScheme($url); // i.e. "https"
echo UrlParser::getSubdomain($url); // i.e. "sub2.sub1"
echo UrlParser::getFragment($url); // i.e. "my-fragment"
echo UrlParser::getSubdomain($url); // i.e. "sub2.sub1"
// see more methods in Class ...
```
