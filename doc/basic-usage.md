# JohnSear URL Utils - Documentation - Basic Usage

## Get Current URL

```php
require __DIR__ . '/vendor/autoload.php';

use JohnSear\UrlUtils\Url;

echo Url::current(); // i.e. "https://www.example.com/path/to/page"

// Alternative using Url as Object
$urlObj = new Url();

echo $urlObj; // i.e. "https://www.example.com/path/to/page"

$url = $urlObj->__toString();
echo $url; // i.e. "https://www.example.com/path/to/page"
```

## Create URL Object with own URL String

```php
require __DIR__ . '/vendor/autoload.php';

use JohnSear\UrlUtils\Url;

$url = 'https://www.example.com/path/to/page';

$urlObj = new Url($url);

echo $urlObj; // i.e. "https://www.example.com/path/to/page"

$url = $urlObj->__toString();
echo $url; // i.e. "https://www.example.com/path/to/page"

echo $urlObj->getScheme(); // i.e. "https"
echo $urlObj->getSubdomain(); // i.e. "www"
echo $urlObj->getPath(); // i.e. "/path/to/page"
// see more methods in Class ...
```

## Build Url by Url Parameters

See [URL Utils - Url Builder - Documentaion](https://bitbucket.org/John_Sear/url-utils/src/master/doc/url-builder.md) for more information.

```php
require __DIR__ . '/vendor/autoload.php';

use JohnSear\UrlUtils\UrlBuilder;

$urlBuilder = new UrlBuilder('https', 'www.example.com');

$urlBuilder->setPath('/path/to/page');
$urlBuilder->addQueryParam('foo', 'bar');
// see more methods in Class ...

echo $urlBuilder->build(); // i.e. "https://www.example.com/path/to/page?foo=bar"
```
---
> Full Documentation can be found [here](https://bitbucket.org/John_Sear/url-utils/src/master/doc/index.md)
