<?php

declare(strict_types=1);

namespace JohnSear\UrlUtils;

class UrlBasicAuthCredentials
{
    /** @var string */
    private $user = '';
    /** @var string */
    private $pass = '';

    /**
     * @param string $user
     * @return UrlBasicAuthCredentials
     */
    public function setUser(string $user): UrlBasicAuthCredentials
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @return string
     */
    public function getUser(): string
    {
        return $this->user;
    }

    /**
     * @param string $pass
     * @return UrlBasicAuthCredentials
     */
    public function setPass(string $pass): UrlBasicAuthCredentials
    {
        $this->pass = $pass;
        return $this;
    }

    /**
     * @return string
     */
    public function getPass(): string
    {
        return $this->pass;
    }
}
