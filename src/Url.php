<?php

declare(strict_types=1);

namespace JohnSear\UrlUtils;

use PhpParser\Node\Stmt\Echo_;

class Url
{
    public const PORT_DEFAULT   = 80;
    public const PORT_SECURE    = 443;
    public const SCHEME_DEFAULT = 'http';
    public const SCHEME_SECURE  = 'https';

    /** @var string */
    private static $scheme     = self::SCHEME_DEFAULT;
    /** @var string */
    private static $host       = '';
    /** @var int */
    private static $port       = self::PORT_DEFAULT;
    /** @var string */
    private static $user       = '';
    /** @var string */
    private static $pass       = '';
    /** @var string */
    private static $path       = '/';
    /** @var string */
    private static $query      = '';
    /** @var string */
    private static $fragment   = '';
    /** @var string */
    private static $subdomain  = '';
    /** @var string */
    private static $domainname = '';
    /** @var string */
    private static $tld        = '';

    public function __construct(string $url = null)
    {
        $url = trim(($url ?? self::current()));

        // parse Url

        self::$scheme     = UrlParser::getScheme($url);;
        self::$user       = UrlParser::getUser($url);
        self::$pass       = UrlParser::getPass($url);
        self::$host       = UrlParser::getHost($url);
        self::$port       = UrlParser::getPort($url);
        self::$path       = UrlParser::getPath($url);
        self::$query      = UrlParser::getQuery($url);
        self::$fragment   = UrlParser::getFragment($url);

        self::$subdomain  = UrlParser::getSubdomain($url);
        self::$domainname = UrlParser::getDomainName($url);
        self::$tld        = UrlParser::getTopLevelDomain($url);
    }

    public static function current(): string
    {
        // Build generic Url
        $s        = $_SERVER;
        $ssl      = (!empty($s['HTTPS']) && $s['HTTPS'] === 'on');
        $sp       = strtolower(($s['SERVER_PROTOCOL'] ?? ''));
        $protocol = substr($sp, 0, (int)strpos($sp, '/')) . (($ssl) ? 's' : '');
        $port     = $s['SERVER_PORT'] ?? self::PORT_DEFAULT;
        $port     = ((!$ssl && $port === self::PORT_DEFAULT) || ($ssl && $port === self::PORT_SECURE)) ? '' : ':' . $port;
        $host     = ($s['HTTP_HOST'] ?? null);
        $host     = $host ?? ($s['SERVER_NAME'] ?? '') . $port;
        $url      = $protocol . '://' . $host . ($s['REQUEST_URI'] ?? '');

        return trim($url);
    }

    /**
     * @return bool
     */
    public function isSecure(): bool
    {
        return (self::$scheme === 'https');
    }

    /**
     * @return string
     */
    public function getScheme(): string
    {
        return self::$scheme;
    }

    /**
     * @return string
     */
    public function getHost(): string
    {
        return self::$host;
    }

    /**
     * @return int
     */
    public function getPort(): int
    {
        return self::$port;
    }

    public function getBaseUrl(): string
    {
        return UrlParser::getBaseUrl($this->__toString());
    }

    /**
     * @return string
     */
    public function getUser(): string
    {
        return self::$user;
    }

    /**
     * @return string
     */
    public function getPass(): string
    {
        return self::$pass;
    }

    /**
     * @return string
     */
    public function getQuery(): string
    {
        return self::$query;
    }

    /**
     * @return array
     */
    public function getQueryParams(): array
    {
        parse_str(self::$query, $queryParams);

        return $queryParams;
    }

    /**
     * @return string
     */
    public function getPath(): string
    {
        return self::$path;
    }

    /**
     * @return string
     */
    public function getFragment(): string
    {
        return self::$fragment;
    }

    /**
     * @return string
     */
    public function getFile(): string
    {
        return UrlParser::getFile($this->__toString());
    }

    /**
     * @return string
     */
    public function getSubdomain(): string
    {
        return self::$subdomain;
    }

    /**
     * @return string[]
     */
    public function getSubdomains(): array
    {
        return explode('.', self::$subdomain);
    }

    /**
     * @return string
     */
    public function getDomainname(): string
    {
        return self::$domainname;
    }

    /**
     * @return string
     */
    public function getDomain(): string
    {
        return UrlParser::getDomain($this->__toString());
    }

    /**
     * @return string
     */
    public function getTld(): string
    {
        return self::$tld;
    }

    public function __toString(): string
    {
        $urlBuilder = (new UrlBuilder())
            ->setScheme($this->getScheme())
            ->setCredentials((new UrlBasicAuthCredentials())->setUser($this->getUser())->setPass($this->getPass()))
            ->setHost($this->getHost())
            ->setPort($this->getPort())
            ->setPath($this->getPath())
            ->setFragment($this->getFragment());

        foreach ($this->getQueryParams() as $key => $value) {
            $urlBuilder->addQueryParam($key, $value);
        }

        return trim($urlBuilder->build());
    }

    /**
     * @return array{ scheme: string, user: string, pass: string, host: string, port: int, path: string, query: string, fragment: string, file: string, subdomain: string, domainname: string, tld: string}
     */
    public function __toArray(): array
    {
        return UrlParser::parseUrl($this->__toString());
    }
}
