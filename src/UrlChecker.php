<?php

declare(strict_types=1);

namespace JohnSear\UrlUtils;

class UrlChecker
{
    public static function isUrlReachable(string $url, int $timeout = 10): bool
    {
        if(!self::isIpFromUrlResolvable($url) || self::getHttpStatusCode($url) !== '200') {
            return false;
        }

        $urlObj = new Url($url);
        $scheme = $urlObj->getScheme();
        $host   = strtolower($urlObj->getHost());

        if ($scheme === '' || $host === '') {
            return false;
        }

        $url = $scheme . '://' . $host;
        $port = Url::PORT_DEFAULT;

        if ($scheme === Url::SCHEME_SECURE) {
            $url = 'ssl://' . $host;
            $port = Url::PORT_SECURE;
        }

        $socket = fsockopen($url, $port, $errno, $errStr, $timeout);

        if ($socket) {
            fclose($socket);
            return true;
        }

        return false;
    }

    public function isUrlValid(string $url): bool
    {
        $urlObj = new Url($url);
        $scheme = $urlObj->getScheme();
        $host = $urlObj->getHost();

        $builtUrl = $scheme . '://' . $host;
        $cleanedUrl = str_replace('://', '', $builtUrl);

        return trim($cleanedUrl) !== '';
    }

    public function isUrlFragmentEqualTo(string $url, string $fragment): bool
    {
        $urlObj = new Url($url);

        return $urlObj->getFragment() === $fragment;
    }

    public function urlQueryKeyExists(string $url, string $queryKey): bool
    {
        $urlObj = new Url($url);
        $queryParams = $urlObj->getQueryParams();

        return array_key_exists($queryKey, $queryParams);
    }

    public static function getHttpStatusCode(string $url): string
    {
        if(!self::isIpFromUrlResolvable($url)) {
            return '';
        }

        $urlObj = new Url($url);
        $scheme = $urlObj->getScheme();
        $host   = $urlObj->getHost();

        $url = ($scheme !== '' && $host !== '') ? $scheme . '://' . $host : $url;

        $headers = get_headers($url);

        if ($headers) {
            $statusCode = substr($headers[0], 9, 3);

            return (string) $statusCode;
        }

        return '';
    }

    public static function isIpFromUrlResolvable(string $url): bool
    {
        $urlObj = new Url($url);
        $host = $urlObj->getHost();

        return ($host !== '' && gethostbyname($host) !== $host);
    }

    /**
     * Total transaction time in seconds
     */
    public static function getCurlTotalTime(string $url, int $timeout = 5): int
    {
        if(!self::isIpFromUrlResolvable($url)) {
            return 0;
        }

        $urlObj = new Url($url);
        $scheme = $urlObj->getScheme();
        $host   = $urlObj->getHost();

        $url = ($scheme !== '' && $host !== '') ? $scheme . '://' . $host : $url;

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);
        curl_exec($ch);
        $totalTime = curl_getinfo($ch, CURLINFO_TOTAL_TIME);
        curl_close($ch);

        return (int) $totalTime;
    }
}
