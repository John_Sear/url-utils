<?php

declare(strict_types=1);

namespace JohnSear\UrlUtils;

class UrlParser
{
    public static function getBaseUrl(string $url = null): string
    {
        $url  = $url ?? Url::current();

        $cleanUrl = str_replace(
            [
                self::getScheme() . '://',
                self::getPath(),
                '?' . self::getQuery(),
                '#' . self::getFragment()
            ],
            '',
            $url
        );

        $baseUrl = self::getScheme() . '://' . rtrim($cleanUrl, '/') . '/';

        return trim($baseUrl);
    }

    /**
     * @return array{ scheme: string, user: string, pass: string, host: string, port: int, path: string, query: string, fragment: string, file: string, subdomain: string, domainname: string, tld: string }
     */
    public static function parseUrl(string $url = null): array
    {
        $url = $url ?? Url::current();

        return [
            'scheme'     => self::getScheme($url),
            'user'       => self::getUser($url),
            'pass'       => self::getPass($url),
            'host'       => self::getHost($url),
            'port'       => self::getPort($url),
            'path'       => self::getPath($url),
            'query'      => self::getQuery($url),
            'fragment'   => self::getFragment($url),

            'file'       => self::getFile($url),

            'subdomain'  => self::getSubdomain($url),
            'domainname' => self::getDomainName($url),
            'tld'        => self::getTopLevelDomain($url)
        ];
    }

    public static function getScheme(string $url = null): string
    {
        $url = $url ?? Url::current();

        $parsedUrl = parse_url($url);

        return $parsedUrl['scheme'] ?? Url::SCHEME_DEFAULT;
    }

    public static function getUser(string $url = null): string
    {
        $url = $url ?? Url::current();

        $parsedUrl = parse_url($url);

        return $parsedUrl['user'] ?? '';
    }

    public static function getPass(string $url = null): string
    {
        $url = $url ?? Url::current();

        $parsedUrl = parse_url($url);

        return $parsedUrl['pass'] ?? '';
    }

    public static function getHost(string $url = null): string
    {
        $url = $url ?? Url::current();

        $parsedUrl = parse_url($url);

        return $parsedUrl['host'] ?? '';
    }

    /**
     * Third-Level-Domain
     */
    public static function getSubdomain(string $url = null): string
    {
        $url = $url ?? Url::current();
        $host = self::getHost($url);
        $subdomains = $host;

        $domain = self::getDomain($url);

        if($domain !== '') {
            $subdomains = rtrim(strstr($subdomains, $domain, true), '.');
        }

        return $subdomains;
    }

    public static function getDomainName(string $url = null): string
    {
        $url = $url ?? Url::current();

        $domain = self::getDomain($url);
        $tld = self::getTopLevelDomain($url);

        return str_replace('.' . $tld, '', $domain);
    }

    /**
     * Second-Level-Domain
     */
    public static function getDomain(string $url = null): string
    {
        $url = $url ?? Url::current();
        $host = self::getHost($url);

        if(preg_match("/(?P<domain>[a-z0-9][a-z0-9\-]{1,63}\.[a-z\.]{2,6})$/i", $host, $matches))
        {
            return $matches['domain'];
        }

        return $host;
    }

    /**
     * Top-Level-Domain
     */
    public static function getTopLevelDomain(string $url = null): string
    {
        $url = $url ?? Url::current();
        $host = self::getHost($url);

        $parts = explode('.', $host);

        $strap = array_reverse($parts);

        $tld = $strap[0] ?? '';

        return ($tld === $host) ? '' : $tld;
    }

    public static function getPort(string $url = null): int
    {
        $url = $url ?? Url::current();

        $parsedUrl = parse_url($url);

        $port = $parsedUrl['port'] ?? Url::PORT_DEFAULT;

        if($port === Url::PORT_DEFAULT && self::getScheme($url) === Url::SCHEME_SECURE) {
            $port = Url::PORT_SECURE;
        }

        if($port === Url::PORT_SECURE && self::getScheme($url) === Url::SCHEME_DEFAULT) {
            $port = Url::PORT_DEFAULT;
        }

        return (int) $port;
    }

    public static function getPath(string $url = null): string
    {
        $url = $url ?? Url::current();

        $parsedUrl = parse_url($url);

        return $parsedUrl['path'] ?? '/';
    }

    public static function getFile(string $url = null): string
    {
        $url = $url ?? Url::current();
        $path = self::getPath($url);
        $query = self::getQuery($url);

        $requestUri = $path . ($query !== '' ? '?' . $query : '');

        return basename($requestUri, "?". $query);
    }

    public static function getQuery(string $url = null): string
    {
        $url = $url ?? Url::current();

        $parsedUrl = parse_url($url);

        return $parsedUrl['query'] ?? '';
    }

    public static function getFragment(string $url = null): string
    {
        $url = $url ?? Url::current();

        $parsedUrl = parse_url($url);

        return $parsedUrl['fragment'] ?? '';
    }
}
