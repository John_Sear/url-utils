<?php

declare(strict_types=1);

namespace JohnSear\UrlUtils;

class UrlBuilder
{
    /** @var string */
    private $scheme;
    /** @var string */
    private $host;
    /** @var UrlBasicAuthCredentials */
    private $credentials;
    /** @var int */
    private $port = 0;
    /** @var string */
    private $path = '/';
    /** @var array */
    private $queryParams = [];
    /** @var string */
    private $fragment = '';

    public function __construct(string $scheme = Url::SCHEME_DEFAULT, string $host = '') {
        $this->scheme = $scheme;
        $this->host = $host;
        $this->credentials = new UrlBasicAuthCredentials();

        if ($scheme === Url::SCHEME_SECURE) {
            $this->port = Url::PORT_SECURE;
        }

        if ($scheme === Url::SCHEME_DEFAULT) {
            $this->port = Url::PORT_DEFAULT;
        }
    }

    public function setScheme(string $scheme): self
    {
        $this->scheme = $scheme;

        return $this;
    }

    public function setCredentials(UrlBasicAuthCredentials $credentials): self
    {
        $this->credentials = $credentials;

        return $this;
    }

    public function setHost(string $host): self
    {
        $this->host = $host;

        return $this;
    }

    public function setPort(int $port): self
    {
        $this->port = $port;

        return $this;
    }

    public function setPath(string $path): self
    {
        $this->path = $path;

        return $this;
    }

    /**
     * @param string $key
     * @param string|array $value
     */
    public function addQueryParam(string $key, $value = ''): self
    {
        $this->queryParams[$key] = $value;

        return $this;
    }

    public function removeQueryParam(string $key): self
    {
        unset($this->queryParams[$key]);

        return $this;
    }

    public function setFragment(string $fragment): self
    {
        $this->fragment = $fragment;

        return $this;
    }

    public function build(): string
    {
        $scheme   = $this->scheme;
        $host     = $this->host;
        $port     = $this->port;
        $user     = $this->credentials->getUser();
        $pass     = $this->credentials->getPass();
        $path     = $this->path;
        $query    = rtrim(str_replace('=&', '&', http_build_query($this->queryParams)), '=');
        $fragment = $this->fragment;

        $ssl  = ($scheme === 'https');
        $port = ($port === 0 || (!$ssl && $port === Url::PORT_DEFAULT) || ($ssl && $port === Url::PORT_SECURE)) ? '' : ':' . $port;
        $host = ($user !== '' && $pass !== '') ? urlencode($user) . ':' . urlencode($pass) . '@' . $host . $port : $host . $port;

        $requestUri = $path . (($query !== '') ? '?' . $query : '') . (($fragment !== '') ? '#' . $fragment : '');
        $url      = $scheme . '://' . rtrim($host, '/') . '/' . ltrim($requestUri, '/');

        return trim($url);
    }
}
