# JohnSear URL Utils

(c) 2023 by [John_Sear](https://bitbucket.org/John_Sear/url-utils/)

> This Library containing useful Utilities to handle, parse, build and check URLs

## Features
* Url _as Object_
* _Build_ Urls
* _Check_ & _Validate_ Urls
* Easily get URL Parts like `Subdomain(s)` or `Query Parameters`
* ...

## Documentation
Full Documentation, i.e. installation with Composer, can be found [here](https://bitbucket.org/John_Sear/url-utils/src/master/doc/index.md)
